% Calculation FFT on a window and return norm-freq.

function [mag, freq] = func_fft(Signal, Fs)
    nfft = max(size(Signal)); % Length of FFT
    mag  = fft(Signal, nfft); 
    mag  = double(abs(mag(1:floor(nfft/2)))); 
    freq = (0:nfft/2-1)*Fs/nfft; 
end
