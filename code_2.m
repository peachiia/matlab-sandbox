clear; clc; close all;
load('./data/Subject1_Nano')

ch1 = data(:, 1);
ch2 = data(:, 2);

sample_rate = 200;

time = 1:length(ch1);
time = time ./ sample_rate;

figure;

signal_resting = ch1((0*sample_rate)+1:(180*sample_rate));
[mag_resting, freq] = func_fft(signal_resting, sample_rate)

signal_working = ch1((200*sample_rate)+1:(380*sample_rate));
[mag_working, freq] = func_fft(signal_working, sample_rate)
plot(freq, mag_working);

resting_alpha_power = 0;
for i = 1441:2341
    resting_alpha_power = resting_alpha_power + mag_resting(i);
end

resting_beta_power = 0;
for i = 2341:5401
    resting_beta_power = resting_beta_power + mag_resting(i);
end

working_alpha_power = 0;
for i = 1441:2341
    working_alpha_power = working_alpha_power + mag_working(i);
end

working_beta_power = 0;
for i = 2341:5401
    working_beta_power = working_beta_power + mag_working(i);
end


fprintf("Resting Alpha = %.4f\n", resting_alpha_power);
fprintf("Resting Beta  = %.4f\n", resting_beta_power);
fprintf("Working Alpha = %.4f\n", working_alpha_power);
fprintf("Working Beta  = %.4f\n", working_beta_power);
