clear; clc;
x = 1:10;
y = x .^ 2;

n = length(y);

for i = 1:n
    fprintf("i = %d :;", i);
    
    if y(i) > 50
        fprintf("y = %d -> YES\n",y(i)); 
    else
        fprintf("y = %d -> NO\n",y(i)); 
    end
end